<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!! csrf_token() !!}" />
    <title>Test Task</title>

    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="Cache-Control" content="post-check=0, pre-check=0" />
    <meta http-equiv="Pragma" content="no-cache" />

    {{--<link href="/css/app.css" rel="stylesheet">--}}
</head>
<body>
    @yield('content')
    <script src="{{ url('/js/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ url('/js/jquery.form.js') }}"></script>
    <script src="{{ url('/js/application.js') }}"></script>
    <script>
        $(document).ready(function(){

            app.init();

        });
    </script>
</body>
</html>