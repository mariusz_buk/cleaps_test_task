@extends('template')

@section('content')

    <h1>Edit</h1>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form id="edit-user-form" method="post" action="{{ route('user.update', $user->id) }}">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="PUT" />
        <table border="1">
            <tbody>
                <tr>
                    <td>First Name:</td>
                    <td><input type="text" name="first-name" value="{{ $user->first_name }}" /></td>
                </tr>
                <tr>
                    <td>Last Name:</td>
                    <td><input type="text" name="last-name" value="{{ $user->last_name }}" /></td>
                </tr>
                <tr>
                    <td>Email address:</td>
                    <td><input type="text" name="email" value="{{ $user->email }}" /></td>
                </tr>
                <tr>
                    <td>Change password of leave empty:</td>
                    <td><input type="password" name="password" /></td>
                </tr>
                <tr>
                    <td>Please repeat the password:</td>
                    <td><input type="password" name="password_confirmation" /></td>
                </tr>
            </tbody>
        </table>

        <br />
        <p>
            Privileges
        </p>
        <table border="1">
            <tbody>
            <tr>
                <td>Resource A</td>
                <td>
                    <label>
                        <input type="checkbox" value="Read from A" name="pv[]" @if (false !== array_search('Read from A', $pvs))checked="checked"@endif />Read
                    </label>
                    <label>
                        <input type="checkbox" value="Write to A" name="pv[]" @if (false !== array_search('Write to A', $pvs))checked="checked"@endif />Write
                    </label>
                </td>
            </tr>
            <tr>
                <td>Resource B</td>
                <td>
                    <label>
                        <input type="checkbox" value="Read from B" name="pv[]" @if (false !== array_search('Read from B', $pvs))checked="checked"@endif />Read
                    </label>
                    <label>
                        <input type="checkbox" value="Write to B" name="pv[]" @if (false !== array_search('Write to B', $pvs))checked="checked"@endif />Write
                    </label>
                </td>
            </tr>
            <tr>
                <td>Resource C</td>
                <td>
                    <label>
                        <input type="checkbox" value="Read from C" name="pv[]" @if (false !== array_search('Read from C', $pvs))checked="checked"@endif />Read
                    </label>
                    <label>
                        <input type="checkbox" value="Write to C" name="pv[]"  @if (false !== array_search('Write to C', $pvs))checked="checked"@endif />Write
                    </label>
                </td>
            </tr>
            </tbody>
        </table>
    </form>

    <div style="margin-top: 10px;">
        <button class="update-user">Update</button>
    </div>