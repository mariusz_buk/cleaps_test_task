@extends('template')

@section('content')

    <h1>Add new user</h1>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <p>
        All fields are required.
    </p>

    <form id="add-user-form" method="post" action="{{ route('user.store') }}">
        {!! csrf_field() !!}
        <table border="1">
            <tbody>
                <tr>
                    <td>First Name:</td>
                    <td><input type="text" name="first-name" value="{{ old('first-name') }}"/></td>
                </tr>
                <tr>
                    <td>Last Name:</td>
                    <td><input type="text" name="last-name" value="{{ old('last-name') }}"/></td>
                </tr>
                <tr>
                    <td>Email address:</td>
                    <td><input type="text" name="email" value="{{ old('email') }}" /></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td><input type="password" name="password" /></td>
                </tr>
                <tr>
                    <td>Please repeat the password:</td>
                    <td><input type="password" name="password_confirmation" /></td>
                </tr>
            </tbody>
        </table>

        <br />
        <p>
            Privileges
        </p>
        <table border="1">
            <tbody>
                <tr>
                    <td>Resource A</td>
                    <td>
                        <label>
                            <input type="checkbox" value="Read from A" name="pv[]" />Read
                        </label>
                        <label>
                            <input type="checkbox" value="Write to A" name="pv[]" />Write
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>Resource B</td>
                    <td>
                        <label>
                            <input type="checkbox" value="Read from B" name="pv[]" />Read
                        </label>
                        <label>
                            <input type="checkbox" value="Write to B" name="pv[]" />Write
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>Resource C</td>
                    <td>
                        <label>
                            <input type="checkbox" value="Read from C" name="pv[]" />Read
                        </label>
                        <label>
                            <input type="checkbox" value="Write to C" name="pv[]"  />Write
                        </label>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

    <div style="margin-top: 10px;">
        <button class="store-user">Add</button>
    </div>

@endsection