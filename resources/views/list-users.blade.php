@extends('template')

@section('content')
    <div>
        <button class="add-user" data-url="{{ route('user.create') }}}">Add new user</button>
    </div>

    <form id="delete-user-form" method="post">
        {!! csrf_field() !!}
        <input type="hidden" id="delete-user-id" name="id" value="" />
        <input type="hidden" name="_method" value="DELETE" />
    </form>

    <div style="margin-top: 10px;">

        <table border="1">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email address</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $key => $user)
                    <tr id="user-{{ $user->id }}">
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $user->first_name }}</td>
                        <td>{{ $user->last_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <button class="edit-user" data-url="{{ route('user.edit', $user->id) }}">Edit</button>
                            <button class="delete-user" data-id="{{ $user->id }}" data-url="{{ route('user.destroy', $user->id) }}">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
