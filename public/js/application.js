/**
 * Created by Mariusz on 19.08.2015.
 */

var app = {

    /**
     * Init Events
     */
    init: function() {
        $(document).on('click', '.add-user', this.addUser);
        $(document).on('click', '.delete-user', this.deleteUser);
        $(document).on('click', '.edit-user', this.editUser);
        $(document).on('click', '.store-user', this.storeUser);
        $(document).on('click', '.update-user', this.updateUser);
    },

    /**
     * Add user form
     */
    addUser: function() {
        var url;

        url = $(this).data('url');
        location.href = url;
    },

    /**
     * Delete user witout warning
     */
    deleteUser: function() {
        var user_id, url;

        user_id = $(this).data('id');
        url = $(this).data('url');
        $('#delete-user-id').val(user_id);

        $('#delete-user-form').ajaxSubmit({
            url: url,
            dataType: 'json',
            success: function(json)
            {
                $('#user-' + user_id).remove();
            }
        });
    },

    /**
     * Open edit user form
     */
    editUser: function() {
        var url;

        url = $(this).data('url');

        location.href = url;
    },

    /**
     * Add new user
     */
    storeUser: function() {
        $('#add-user-form').submit();
    },

    /**
     * Update user
     */
    updateUser: function() {
        $('#edit-user-form').submit();
    }
};