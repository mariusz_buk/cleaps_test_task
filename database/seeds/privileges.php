<?php

use Illuminate\Database\Seeder;

class privileges extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Read from A',
            'Write to A',
            'Read from B',
            'Write to B',
            'Read from C',
            'Write to C'
        ];

        foreach ($data as $d) {
            $p = new \App\Http\Models\Privileges();
            $p->name = $d;
            $p->save();
        }
    }
}
