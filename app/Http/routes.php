<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// routing for api
Route::group(['prefix' => 'api/v1'], function () {
    Route::resource('/users', '\App\Http\Controllers\UsersApi');
});

Route::resource('/user', '\App\Http\Controllers\Users');

Route::get('/', '\App\Http\Controllers\Users@index');