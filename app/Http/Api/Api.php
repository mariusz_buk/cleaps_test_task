<?php

namespace App\Http\Api;

use App\Http\Models\User;

class Api {

    public static function getAllUsers()
    {
        $users = [];
        foreach (User::all() as $key => $user)
        {
            $users[] = self::filteredData($user);
        }

        return $users;
    }

    private static function filteredData($user)
    {
        $privileges = [];
        foreach ($user->privileges->all() as $pv)
        {
            $privileges[] = $pv->name;
        }

        $result = [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'privileges' => $privileges
        ];

        return $result;
    }

    public static function errorMessage()
    {
        return [
            'error' => [
                'error_code' => 400,
                'error_message' => 'Method not available'
            ]
        ];
    }
}
