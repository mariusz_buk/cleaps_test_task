<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Privileges extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'privileges';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

//    public function users()
//    {
//        return $this->belongsToMany('App\Http\Models\User');
//    }
}
