<?php

namespace App\Http\Controllers;

use App\Http\Models\Privileges;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\User;
use Illuminate\Support\Facades\Validator;

class Users extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('list-users', ['users' => User::all()]  );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('create-user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $form = \Input::all();

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'first-name' => 'required',
            'last-name' => 'required',
            'password' => 'required|confirmed'
        ]);

        if ($validator->fails()) {
            return redirect('user/create')
                ->withErrors($validator)
                ->withInput();
        }

        $user = new User();
        $user->email = $form['email'];
        $user->first_name = $form['first-name'];
        $user->last_name = $form['last-name'];
        $user->password = $form['password'];
        $user->save();

        if (isset($form['pv']) && is_array($form['pv'])) {
            $privileges = Privileges::all();
            foreach ($privileges as $p) {
                if (false !== array_search($p->name, $form['pv'])) {
                    $pivot = [$p->id => ['created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")]];
                    $user->privileges()->attach($pivot);
                }
            }
        }

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        if (! $user)
        {
            return redirect('/');
        }

        $pvs = [];
        foreach ($user->privileges as $p)
        {
            $pvs[] = $p->name;
        }

        return view('edit-user', ['user' => $user, 'pvs' => $pvs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $form = \Input::all();

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'first-name' => 'required',
            'last-name' => 'required',
            'password' => 'confirmed'
        ]);

        if ($validator->fails()) {
            return redirect("user/{$id}/edit")
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::find($id);

        if (! $user)
        {
            return redirect("user/{$id}/edit")
                ->with('id', $id)
                ->withInput();
        }

        $user->email = $form['email'];
        $user->first_name = $form['first-name'];
        $user->last_name = $form['last-name'];

        if (!empty($form['password'])) {
            $user->password = $form['password'];
        }
        $user->save();

        $pvs = [];
        if (isset($form['pv']) && is_array($form['pv'])) {
            $privileges = Privileges::all();
            foreach ($privileges as $p) {
                if (false !== array_search($p->name, $form['pv'])) {
                    $pvs[] = $p->id;
                }
            }
        }
        $user->privileges()->sync($pvs);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (! $user)
        {
            return redirect('/');
        }

        // remove related privileges
        $user->privileges()->sync([]);

        // remove user
        $user->delete();

        return [ 'id' => $id ];
    }
}
